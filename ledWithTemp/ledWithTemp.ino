#include <Wire.h>
#include <Adafruit_MLX90614.h>


Adafruit_MLX90614 mlx = Adafruit_MLX90614();

int redPin = 4;
int greenPin = 2;
int bluePin = 3;
double ambientTemp;
double objectTemp;
//double current_temp;
//double prev_temp;
int delaytime = 1000;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Adafruit MLX90614 test");  
  mlx.begin();  
  
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  /*  amibent = object -> red LED
   *   
   *  WILL NEED AN INTIAL  < OR > ~80F TO SPLIT HOT AND COLD
   *  object temp > ambient coffee mode
   *  
   *  object temp < ambisent beer mode
   *  
   *  
   *  COFFE TEA TEMPS 
   *  ??if temp previous < temp current ignore (sensor warming up)
   * RED            LED < 115f
   * yellow + green LED < 120f
   * green          LED < 140f i.e 120-140f DRINKABLE
   * red            LED > 140f 
   * 
   * BEER TEMPS
   * ?? if temp_prev > temp_current ignore (sensore cooling down from room temp)
   * red    LED < 42f   too cold
   * green  LED < 52f   ideal range
   * yellow LED > 52f   maybe too warm (prob still drinkable)
   */
  
  ambientTemp = mlx.readAmbientTempF();
  objectTemp = mlx.readObjectTempF();

  int roomTemp = 70;
  int green_led = 0;
  int yellow_led = 1;
  int red_led = 2;
  
  if (ambientTemp == objectTemp)
  {
    // Object is at room temp
    setColor(255, 0, 0); // red
  }
  else if (objectTemp > ambientTemp)
  {
    // Coffee/Tea mode 
    if (objectTemp < 115)
    {
      // too cold
      setColor(255, 0, 0); // red
    }
    else if (objectTemp < 120)
    {
      // luke warm
      // 115 <= Temp < 120
      setColor(255, 255, 0); // yellow
    }
    else if (objectTemp <= 140)
    {
      // 120 <= temp <= 140 IDEAL TEMP
      setColor(0, 255, 0); // green
    }
    else
    {
      // too hot
      setColor(255, 0, 0); // red
    }
  }
  else if(objectTemp < ambientTemp)
  {
    // Beer Mode
    if (objectTemp < 42)
    {
      setColor(255, 0, 0); // red
    }
    else if(objectTemp < 52)
    {
      setColor(0, 255, 0); // green
    }
    else
    {
      setColor(255, 255, 0); // yellow
    }
  }

  
  delay(delaytime);
  Serial.print("Ambient Temp: ");Serial.print(ambientTemp);Serial.println("*F");
  Serial.print("Object Temp: ");Serial.print(objectTemp);Serial.println("*F");
}

/*void toggle_led(int led_num)
{
    digitalWrite(ledPins[led_num],HIGH); //Turn on LED @ Pos num
    delay(delaytime);
    digitalWrite(ledPins[led_num],LOW);  
}*/

void setColor(int red, int green, int blue)
{
#ifdef COMMON_ANODE
red = 255 - red;
green = 255 - green;
blue = 255 - blue;
#endif
analogWrite(redPin, red);
analogWrite(greenPin, green);
analogWrite(bluePin, blue);
}
