int ledPins[] = {2,3,4};
int num;
int tempPin = A0; // Arduino Pin to read sensor
int sensorInput;  // var to store sensor input
double temp;      // var to store temp in degrees

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); //start serial port  
  int index;
  for(index=0;index<=2;index++)
  {
    pinMode(ledPins[index],OUTPUT);
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  //num = random(3); 
  //Will eventually be temp sensor
  sensorInput = analogRead(A0);
  temp = (double)sensorInput /1024;//find % of input reading 
  temp = temp * 5;                 //multiply by 5v fot voltage
  temp = temp - 0.5;               // subtract offset
  temp = temp * 100;               // Convert to degrees C
  
  int delaytime = 1000;
  
  if(temp > 28)
  {
    digitalWrite(ledPins[2],HIGH); //Turn on LED @ Pos num
    delay(delaytime);
    digitalWrite(ledPins[2],LOW);
  }
  else
  {
    digitalWrite(ledPins[0],HIGH); //Turn on LED @ Pos num
    delay(delaytime);
    digitalWrite(ledPins[0],LOW);
  }

  Serial.print("Current Temp: ");
  Serial.print(temp);
  Serial.print("\n");

}
